import os
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

# def valida_PDF(value):
    
#     ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
#     valid_extensions = ['.pdf']
#     if not ext.lower() in valid_extensions:
#         raise ValidationError('Sólo archivos en PDF.')

def valida_FOTO(value):
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.jpg', '.png', '.jpeg']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Las fotos sólo pueden subirse en JPG, JPEG o PNG.')

# def limite_PDF(value): 
#     limite = 50 * 1024 * 1024
#     if value.size > limite:
#         raise ValidationError('El archivo es muy pesadooo. Tengo una Raspberry amigo, no estamos en lo servidores de AWS o Guguel, no seas tan golos@ X)')

def limite_FOTO(value): 
    limite = 5 * 1024 * 1024
    if value.size > limite:
        raise ValidationError('La imagen es muy pesada. ¿Se podrá reducir?)')