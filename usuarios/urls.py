from django.urls import path


from . import views
from .views import UsuarioRegistrar


urlpatterns = [
    #path('', views.inicio, name="inicio"),
    path("ingreso", views.ingresoUsuario, name="ingreso"),
     path("salir", views.saleUsuario, name="salir"),
    path('registro', UsuarioRegistrar.as_view(), name='usuario_registrar'),
    path("zona_usuario", views.zonaUsuario, name="zona_usuario"),
    # path('detalle/<int:pk>', InternoDetalle.as_view(), name='interno_detalle'),
    
    
]
