from django.db import models

from django.conf import settings
from django.utils import timezone

#from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser

from .validador import *


class Usuarios(AbstractUser):
    foto = models.ImageField(upload_to="imagen_usuarios", null=False, blank=True, verbose_name='Imagen', validators=[valida_FOTO, limite_FOTO])
    direccion = models.CharField(null=True, max_length=150)
    ciudad = models.CharField(null=True, max_length=100)
    telefono = models.CharField(max_length=15,null=True)
    codigo_postal = models.CharField(max_length=6,null=True)
    dni = models.CharField(max_length=20,null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        permissions = (('administracion', 'Sector de administracion'),
                       ('pacientes', 'Permiso para pacientes'),  )
    