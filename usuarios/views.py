from django.shortcuts import render

from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)

from django.urls import reverse_lazy
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

#from django.contrib.auth.models import User
#from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Usuarios
from usuarios.forms import FormuRegistroUsuario 

from django.contrib import messages

from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm

def ingresoUsuario(request):
	if request.method == "POST":
        
		form = AuthenticationForm(request, data=request.POST)
        #remember_me = forms.BooleanField(required=False) #esto va en forms.py
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"Bienvenido {username}.")
				return redirect("zona_usuario")
			else:
				messages.error(request,"Usuario o clave inválida.")
		else:
			messages.error(request,"Usuario o clave inválida.")
	form = AuthenticationForm()
	return render(request=request, template_name="usuario-ingreso.html", context={"login_form":form})

@login_required
def zonaUsuario(request):
    return render (request=request, template_name="zona-usuario.html")

def saleUsuario (request):
    logout(request)
    messages.success(request, f'Saliste de tu sesión correctamente.')
    return redirect ('/')


class UsuarioRegistrar(CreateView):
    form_class = FormuRegistroUsuario
    model = Usuarios
    template_name = 'usuario-registrar.html'
    #fields = ['first_name', 'last_name', 'username', 'email','foto', 'dni', 'direccion', 'ciudad', 'codigo_postal', 'telefono', 'password1', 'password2']
    context_object_name = 'usuario_registrar'
    login_url = 'ingreso/'
    redirect_field_name = 'redirect_to'
    

# class InternoActualizar(LoginRequiredMixin,UpdateView):
#     model = User
#     template_name = 'usuario-editar.html'
#     fields = ['nombre', 'cargo', 'interno', 'dependencia_id']
#     context_object_name = 'interno_actualizar'
#     success_url = reverse_lazy('internos_lista')
#     login_url = '/admin/'
#     redirect_field_name = 'redirect_to'
    
#     def form_valid(self, form):
#         messages.success(self.request, "¡La info fue actualizada!")
#         super().form_valid(form)
#         #return HttpResponseRedirect(self.get_success_url())
#         return redirect ('internos_lista')
    
# class InternoBorrar(LoginRequiredMixin,DeleteView):
#     model = User
#     template_name = 'confirma-borrado.html'
#     context_object_name = 'interno_borrar'
#     success_url = reverse_lazy('internos_lista')
#     login_url = '/admin/'
#     redirect_field_name = 'redirect_to'

#     def form_valid(self, form):
#         messages.success(self.request, "¡La info fue borrada!")
#         super().form_valid(form)
#         #return HttpResponseRedirect(self.get_success_url())
#         return redirect ('internos_lista')


### VER ESTO PARA EL LOGUEO
# from django.contrib.auth import login as auth_login
# class UpdatedLoginView(LoginView):
#     form_class = LoginForm
    
#     def form_valid(self, form):
       
#         remember_me = form.cleaned_data['remember_me']  # get remember me data from cleaned_data of form
#         if not remember_me:
#             self.request.session.set_expiry(0)  # if remember me is 
#             self.request.session.modified = True
#         return super(UpdatedLoginView, self).form_valid(form)

# request.session.set_expiry(1209600) # 2 weeks