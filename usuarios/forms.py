from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Usuarios

class FormuRegistroUsuario(UserCreationForm):
    username = forms.CharField(label='Nombre de usuario', widget=forms.TextInput(attrs={"class":"form-control"})) 
    email = forms.EmailField(label='Direccion de correo', widget=forms.EmailInput(attrs={"class":"form-control"}))
    password1 =forms.CharField(label='Clave', widget=forms.PasswordInput(attrs={"class":"form-control"}))
    password2 =forms.CharField(label='Confirmar Clave', widget=forms.PasswordInput(attrs={"class":"form-control"}))
    
    class Meta:
        model = Usuarios
        fields = ['first_name', 'last_name', 'username', 'email','foto', 'dni', 'direccion', 'ciudad', 'codigo_postal', 'telefono', 'password1', 'password2']
        help_texts = {k:"" for k in fields }




     