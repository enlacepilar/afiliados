from django.contrib import admin

# Register your models here.


from django.contrib.auth.admin import UserAdmin

from .models import Usuarios

class UsuariosAdmin(UserAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff',
         'foto', 'direccion', 'ciudad', 'telefono', 'codigo_postal', 'dni', 'activo', 'password'
        )

admin.site.register(Usuarios, UsuariosAdmin)