# Afiliados-django
Proyecto de turnero y solicitud de aprobación de ordenes con registro de usuarios escrito en Django.

## Cronograma
12/11/2022: Te podés Registrar e ingresar
13/11/2022: Creo las aplicaciones y modelos turnero y especialidad



Para clonar 
git clone https://gitlab.com/enlacepilar/afiliados.git

1) Instalar Venv para entornos virtuales desde la consola:

  sudo apt install python3-venv

2)Crear la carpeta con el entorno virtual

  python3 -m venv tutorial

3)Activar el entorno virtual

  source tutorial/bin/activate

4) Installar django

  python -m pip install django

5)Generar la carpeta de Django

  django-admin startproject principal

6) Ingresar en la carpeta blog e inicializar la aplicacion poll

  python manage.py startapp pagina_principal

7) Para importar si existen las dependencias en otro lado, otro equipo, etc

  pip install -r requirements.txt


