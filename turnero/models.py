from django.db import models

from usuarios.models import Usuarios
from especialidad.models import Especialidad
from django.utils import timezone
#from datetime import datetime, date, time

class Turnero (models.Model):
    """Modelo para registrar_visita usuario"""
    day  = timezone.now()
    hour = timezone.now()
    #formatedHour = hour.strftime("%Y/%m/%d %H:%M:%S")
    formatedDay  = day.strftime("%Y/%m/%d")
    formatedHour = hour.strftime("%H:%M:%S")

    usuario_id = models.ForeignKey(Usuarios, null=False, on_delete=models.DO_NOTHING)
    especialidad_id = models.ForeignKey(Especialidad, null=False, on_delete=models.DO_NOTHING)
    dia = models.DateField(null=False)
    hora = models.TimeField(null=False) 
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_modificacion = models.DateTimeField(
        blank=True, null=True
    )

    def __str__(self):
          
        return str(self.usuario_id)
