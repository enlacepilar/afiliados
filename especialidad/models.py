from django.db import models

from django.utils import timezone

class Especialidad (models.Model):
    nombre = models.CharField(max_length=255, blank=False, null=False)
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_modificacion = models.DateTimeField(
        blank=True, null=True
    )

    def __str__(self):
          
          return self.nombre
